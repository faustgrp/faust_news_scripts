import numpy as np
from pylab import *
from test_butterfly_prod import (VEC_TIME, MAT_TIME, DFT_TIME, OPT_DFT_TIME,
                                 dims, nvec_samples, nmat_samples)
from itertools import product

times = np.load('dft_opt_time.npz')['times']

labels_f, labels_op = {}, {}
labels_f[DFT_TIME] = 'F@'
labels_f[OPT_DFT_TIME] = 'oF@'
labels_op[VEC_TIME] = 'v'
labels_op[MAT_TIME] = 'M'
misc_infos = {}
misc_infos[MAT_TIME] = "Faust-matrix multiplications\n M.shape[1] = 64"
misc_infos[VEC_TIME] = "Faust-vector multiplications"

suptitle('DFT Diagonal Optimization Benchmark\noF: optimized Faust DFT, F: '
         ' baseline Faust DFT', fontweight='bold')

for j, nsamples in zip([VEC_TIME, MAT_TIME], [nvec_samples, nmat_samples]):
    subplot(120+j+1)
    title(str(nsamples)+" "+misc_infos[j])
    for k in [DFT_TIME, OPT_DFT_TIME]:
        semilogy(dims[3:], times[3:, j, k], label=labels_f[k]+labels_op[j], marker='+')

    ylabel("cumulative time (s)")
    xlabel("log2(F.shape[0])")
    legend()
    grid(True)
    xticks(dims[3:], dims[3:], rotation=-30, fontsize=8)

tight_layout()
savefig("dft_opt_times.png")
