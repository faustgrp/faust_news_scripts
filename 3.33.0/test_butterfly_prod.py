from pyfaust import dft
from timeit import timeit
import numpy as np

dims = list(range(3, 21))
VEC_TIME, MAT_TIME = 0, 1
DFT_TIME, OPT_DFT_TIME = 0, 1
nvec_samples = 1000
nmat_samples = 10


if __name__ == '__main__':
    times = np.zeros((len(dims), 2, 2))

    for i, log2size in enumerate(dims):
        print("="*5, log2size)
        size = 2**log2size
        F = dft(size, normed=False)
        oF = dft(size, normed=False, diag_opt=True)
        x = np.random.rand(size)
        M = np.asfortranarray(np.random.rand(size, 64))
        t1 = times[i, VEC_TIME, DFT_TIME] = timeit(lambda:F@x, number=nvec_samples)
        t2 = times[i, VEC_TIME, OPT_DFT_TIME] = timeit(lambda:oF@x, number=nvec_samples)
        t3 = times[i, MAT_TIME, DFT_TIME] = timeit(lambda:F@M, number=nmat_samples)
        t4 = times[i, MAT_TIME, OPT_DFT_TIME] = timeit(lambda:oF@M, number=nmat_samples)

        #assert(np.allclose(F@x, oF@x))
        print("F@x:", ("%.3f" % t1))
        print("oF@x:", ("%.3f" % t2))
        print("F@M:", ("%.3f" % t3))
        print("oF@M:", ("%.3f" % t4))
        np.savez('dft_opt_time.npz', times=times)

