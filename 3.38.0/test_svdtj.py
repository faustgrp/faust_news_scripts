from pyfaust.fact import svdtj
from scipy.sparse import spdiags
from time import time
import numpy as np
from os import environ

np.random.seed(42)

M = np.random.rand(128, 256)

# environ['SVDTJ_ALL_TRUE_ERR'] = '1' # for disabling two-times error strategy

for e in range(1, 6):
    tol = 10**-e
    t1 = time()
    U3, S3, V3 = svdtj(M, tol=tol, enable_large_Faust=False, err_period=10)
    t2 = time()
    S3_ = spdiags(S3, [0], U3.shape[0], V3.shape[0])
    err = np.linalg.norm(U3 @ S3_ @ V3.H - M) / np.linalg.norm(M)
    print("tol:", tol, "err:", err, "time:", t2-t1)

