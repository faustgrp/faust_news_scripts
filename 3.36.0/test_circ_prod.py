from pyfaust import circ
from timeit import timeit
import numpy as np

dims = list(range(3, 21))
VEC_TIME, MAT_TIME = 0, 1
CIRC_TIME, OPT_CIRC_TIME = 0, 1
nvec_samples = 1000
nmat_samples = 10


if __name__ == '__main__':
    times = np.zeros((len(dims), 2, 2))

    for i, log2size in enumerate(dims):
        print("="*5, log2size)
        size = 2**log2size
        v = np.random.rand(size)
        F = circ(v)
        oF = circ(v, diag_opt=True)
        x = np.random.rand(size)
        M = np.asfortranarray(np.random.rand(size, 64))
        t1 = times[i, VEC_TIME, CIRC_TIME] = timeit(lambda:F@x, number=nvec_samples)
        t2 = times[i, VEC_TIME, OPT_CIRC_TIME] = timeit(lambda:oF@x, number=nvec_samples)
        t3 = times[i, MAT_TIME, CIRC_TIME] = timeit(lambda:F@M, number=nmat_samples)
        t4 = times[i, MAT_TIME, OPT_CIRC_TIME] = timeit(lambda:oF@M, number=nmat_samples)

        assert(np.allclose(F@x, oF@x))
        assert(np.allclose(F@M, oF@M))
        print("F@x:", ("%.3f" % t1))
        print("oF@x:", ("%.3f" % t2))
        print("F@M:", ("%.3f" % t3))
        print("oF@M:", ("%.3f" % t4))
        np.savez('circ_opt_time.npz', times=times)

