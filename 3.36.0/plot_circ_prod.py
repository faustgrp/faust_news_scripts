import numpy as np
from pylab import *
from test_circ_prod import (VEC_TIME, MAT_TIME, CIRC_TIME, OPT_CIRC_TIME,
                                 dims, nvec_samples, nmat_samples)
from itertools import product

times = np.load('circ_opt_time.npz')['times']

labels_f, labels_op = {}, {}
labels_f[CIRC_TIME] = 'F@'
labels_f[OPT_CIRC_TIME] = 'oF@'
labels_op[VEC_TIME] = 'v'
labels_op[MAT_TIME] = 'M'
misc_infos = {}
misc_infos[MAT_TIME] = "Faust-matrix multiplications\n M.shape[1] = 64"
misc_infos[VEC_TIME] = "Faust-vector multiplications"

suptitle('Circ Optimization Benchmark\noF: optimized Faust Circ, F: '
         ' baseline Faust Circ', fontweight='bold')

for j, nsamples in zip([VEC_TIME, MAT_TIME], [nvec_samples, nmat_samples]):
    subplot(120+j+1)
    title(str(nsamples)+" "+misc_infos[j])
    for k in [CIRC_TIME, OPT_CIRC_TIME]:
        semilogy(dims[3:], times[3:, j, k], label=labels_f[k]+labels_op[j], marker='+')

    ylabel("cumulative time (s)")
    xlabel("log2(F.shape[0])")
    legend()
    grid(True)
    xticks(dims[3:], dims[3:], rotation=-30, fontsize=8)

tight_layout()
savefig("circ_opt_times.png")
