from pyfaust import circ, anticirc, toeplitz
from scipy.linalg import circulant, toeplitz as stoeplitz
import numpy as np
from time import time
from sys import argv
from os.path import basename

def anticirculant(c):
    P = np.zeros((len(v), len(v)))
    I = np.arange(len(v)-1, -1, -1)
    J = np.arange(0, len(v))
    P[I, J] = 1
    return circ(c)@P

def measure_time(func, repeat=3):
    times = np.zeros((repeat))
    for i in range(repeat):
        t1 = time()
        func()
        t2 = time()
        times[i] = t2 - t1
    return np.median(times)

if __name__ == '__main__':

    log2_dims = range(5, 14)
    times = {}
    times['pyfaust_circ'] = np.empty(len(log2_dims))
    times['scipy_circ'] = np.empty(len(log2_dims))
    times['pyfaust_anticirc'] = np.empty(len(log2_dims))
    times['scipy_anticirc'] = np.empty(len(log2_dims))
    times['pyfaust_toeplitz'] = np.empty(len(log2_dims))
    times['scipy_toeplitz'] = np.empty(len(log2_dims))

    for i, l2d in enumerate(log2_dims):
        dim = 2**l2d
        print("dim:", dim)
        v = np.random.rand(dim)
        C = circ(v)
        sC = circulant(v)
        T = toeplitz(v)
        sT = stoeplitz(v)
        A = anticirc(v)
        sA = anticirculant(v)
        times['pyfaust_circ'][i] = measure_time(lambda: C@v)
        times['scipy_circ'][i] = measure_time(lambda: sC@v)
        times['pyfaust_anticirc'][i] = measure_time(lambda: A@v)
        times['scipy_anticirc'][i] = measure_time(lambda: sA@v)
        times['pyfaust_toeplitz'][i] = measure_time(lambda: T@v)
        times['scipy_toeplitz'][i] = measure_time(lambda: sT@v)
    np.savez(basename(argv[0]).replace('.py', '')+'.npz', times=times, log2_dims=log2_dims)
