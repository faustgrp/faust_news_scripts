from pyfaust import dst, dct
import numpy as np
from time import time
from sys import argv
from os.path import basename

def anticirculant(c):
    P = np.zeros((len(v), len(v)))
    I = np.arange(len(v)-1, -1, -1)
    J = np.arange(0, len(v))
    P[I, J] = 1
    return circ(c)@P

def measure_time(func, repeat=3):
    times = np.zeros((repeat))
    for i in range(repeat):
        t1 = time()
        func()
        t2 = time()
        times[i] = t2 - t1
    return np.median(times)

if __name__ == '__main__':

    log2_dims = range(5, 14)
    times = {}
    times['pyfaust_dst'] = np.empty(len(log2_dims))
    times['dense_dst'] = np.empty(len(log2_dims))
    times['pyfaust_dct'] = np.empty(len(log2_dims))
    times['dense_dct'] = np.empty(len(log2_dims))

    for i, l2d in enumerate(log2_dims):
        dim = 2**l2d
        print("dim:", dim)
        v = np.random.rand(dim)
        DST = dst(dim)
        DCT = dct(dim)
        dDST = DST.toarray()
        dDCT = DCT.toarray()
        times['pyfaust_dst'][i] = measure_time(lambda: DST@v)
        times['dense_dst'][i] = measure_time(lambda: dDST@v)
        times['pyfaust_dct'][i] = measure_time(lambda: DCT@v)
        times['dense_dct'][i] = measure_time(lambda: dDCT@v)
    np.savez(basename(argv[0]).replace('.py', '')+'.npz', times=times, log2_dims=log2_dims)
