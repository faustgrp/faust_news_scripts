from pylab import *
from sys import argv
from os.path import basename

if __name__ == '__main__':
    data = load(basename(argv[0]).replace('_plot.py', '')+'.npz',
                allow_pickle=True)
    times = data['times'].item()
    log2_dims = data['log2_dims']
    print("times:", times)
    print("log2_dims:", log2_dims)
#    print(type(times['pyfaust_dst']))
    plt.rcParams['figure.figsize'] = [12, 8]
    suptitle('circ, anticirc, toeplitz benchmark\n Faust@vec versus'
             ' mat@v', fontsize=20,
             fontweight='bold')
    for i, type in enumerate(['circ', 'toeplitz', 'anticirc']):
        subplot(13*10+i+1)
        grid(True)
        plot(log2_dims, times['scipy_'+type], label='scipy_'+type+' mat@v', marker='+')
        plot(log2_dims, times['pyfaust_'+type], label='pyfaust_'+type+' F@v',
             marker='+')
        xlabel('log2(Faust.shape[0])')
        if i == 0:
            ylabel("computation median time (s) for 3 runs")
        legend()
    savefig(basename(argv[0]).replace('_plot.py', '')+'.png')
