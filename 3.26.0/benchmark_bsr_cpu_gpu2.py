from pyfaust import rand_bsr, Faust
from sys import argv
import numpy as np
from time import time
from os.path import basename, exists

def measure_time(func, repeat=3):
    times = np.zeros((repeat))
    for i in range(repeat):
        t1 = time()
        func()
        t2 = time()
        times[i] = t2 - t1
    return np.median(times)

def usage(argv):
    print("USAGE:", argv[0], " cpu|gpu <log2_maxd> [<log2_mind>]")
    print("default log2_mind:", log2_mind)

if __name__ == '__main__':
    cpu_faust_file = 'cpu_bsr_faust'
    log2_mind = 5
    if len(argv) > 1:
        dev = argv[1]
        if len(argv) > 2:
            log2_maxd = int(argv[2])
            if len(argv) > 3:
                log2_mind = int(argv[3])
        else:
            usage(argv)
            exit(2)
    else:
        usage(argv)
        exit(1)
    if dev not in ['cpu', 'gpu']:
        raise ValueError('first argument must be cpu or gpu')
    if log2_mind > log2_maxd:
        tmp = log2_mind
        log2_mind = log2_maxd
        log2_maxd = tmp
    if log2_maxd > 20:
        print("error: avoid log2_maxd > 20")
    # dimension 0: 0 cpu, 1 gpu
    toarray_times = np.zeros((log2_maxd-log2_mind+1))
    FM_times = np.zeros((log2_maxd-log2_mind+1))
    Fv_times = np.zeros((log2_maxd-log2_mind+1))
    for log2_d in range(log2_mind, log2_maxd+1):
        d = 2**log2_d
        bsize = d // 8
        F = rand_bsr(d, d, bsize, bsize, density=.6, dev=dev, num_factors=20)
        print(F)
        M = np.random.rand(F.shape[1], F.shape[1])
        j = log2_d - log2_mind
        toarray_times[j] = measure_time(lambda: F.toarray())
        FM_times[j] = measure_time(lambda: F@M)
        Fv_times[j] = measure_time(lambda: F@M[:,0])
        print("d=", d, "toarray_time=", toarray_times[j], "FM_time=",
              FM_times[j],"Fv_time=", Fv_times[j])
    out_file = basename(argv[0])
    out_file = out_file[:out_file.index('.')]+'-'+dev+'.npz'
    np.savez(out_file, toarray_times=toarray_times, FM_times=FM_times,
             Fv_times=Fv_times)
