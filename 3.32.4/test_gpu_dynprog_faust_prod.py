import pyfaust as pf
from timeit import timeit
from scipy.sparse import bsr_matrix
from scipy.sparse import random
import numpy as np

F1 = pf.rand(1024, 1024, num_factors=32, dim_sizes=[1, 1024], fac_type='dense', dev='cpu');
F2 = pf.rand(1024, 1024, num_factors=32, dim_sizes=[1, 1024], fac_type='sparse', dev='cpu', density=.5)
F3 = pf.Faust([bsr_matrix(random(1024, 512, .2, format='csr'), blocksize=(2, 2)), bsr_matrix(random(512, 256, .1, format='csr'), blocksize=(2, 2)), bsr_matrix(random(256, 16, .01, format='csr'), blocksize=(2, 2)), bsr_matrix(random(16, 4, .9, format='csr')), bsr_matrix(random(4, 1024, .05, format='csr'))])
F4 = pf.Faust([bsr_matrix(random(1024, 512, .2, format='csr'), blocksize=(16, 16)), np.random.rand(512, 512), bsr_matrix(random(512, 256, .1, format='csr'), blocksize=(16, 16)), bsr_matrix(random(256, 16, .01, format='csr'), blocksize=(16, 16)), bsr_matrix(random(16, 4, .9, format='csr'), blocksize=(2,2)), bsr_matrix(random(4, 1024, .05, format='csr'), blocksize=(2,2)),np.random.rand(1024, 1024) ])
F4 = F4@F4.T

for i, F in enumerate([F1, F2, F3, F4]):
    F = F.clone(dev='gpu')
    print(F)
    oF = F.optimize_time()
    print("default left-to-right product/toarray (gpu F"+str(i+1)+"):", timeit(lambda: F.toarray(), number=100))
    print("dynamic programming product/toarray (gpu F"+str(i+1)+"):", timeit(lambda: oF.toarray(), number=100))
