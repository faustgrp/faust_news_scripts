from pyfaust import wht, Faust, __version__
from random import randint
import argparse
import numpy as np
from scipy.sparse import csr_matrix
from time import time
from time import process_time
from sys import argv
from os.path import basename

def measure_func_time(f, nruns):
    """
    Measures the mean execution time of f running it nruns times.
    """
    t1 = time_func()
    for _ in range(nruns):
        f()
    t2 = time_func()
    return (t2 - t1) / nruns

def scipy_faust_mul_slice(scipy_F, m, slice_len):
    """
    Implements scipy F[:, :slice_len]@m
    """
    out = scipy_F[-1][:, :slice_len]@m
    for i in range(len(scipy_F)-2, -1, -1):
        f = scipy_F[i]
        out = f@out
    return out

def scipy_faust_mul_index(scipy_F, m, inds):
    """
    Implements scipy F[:, inds]@m
    """
    out = scipy_F[-1][:, inds]@m
    for i in range(len(scipy_F)-2, -1, -1):
        f = scipy_F[i]
        out = f@out
    return out


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Benchmark Faust-indexing/slicing'
                                     ' against Faust-vector multiplication.')
    parser.add_argument('--min-dim', type=int, default=9,
                        help='int to set the min size of the'
                        ' Faust tested 2**min_dim.')
    parser.add_argument('--max-dim', type=int, default=15,
                        help='int to set the max size of the'
                        ' Faust tested 2**max_dim.')
    parser.add_argument('--nruns', type=int, default=10, help='number of times'
                        ' the multiplication is ran on a Faust')
    parser.add_argument('--time-func', type=str, default='time', help='The'
                        ' method to use for time measurement', choices=['time',
                                                                      'process_time'])
    parser.add_argument('--max-id-len', type=int, default='90', help=''
                        'The maximum length of slicing/indexing (% of '
                        'Faust.shape[1])')
    parser.add_argument('--min-id-len', type=int, default='10', help=''
                        'The minimum length of slicing/indexing (% of '
                        'Faust.shape[1])')
    parser.add_argument('--id-len-step', type=int, default='15', help=''
                        'The step to change the length of slicing/indexing (to'
                        ' go from min-id-len to max-id-len)')

    opts = parser.parse_args()
    time_func = eval(opts.time_func)
    ndims = opts.max_dim-opts.min_dim+1
    idl_step = opts.id_len_step # slice/index length step
    id_lens = list(range(opts.min_id_len,
                              opts.max_id_len+1, idl_step)) # percents
    num_ids = (opts.max_id_len-opts.min_id_len)//idl_step + 1

    F_slicing_times = \
            np.empty((ndims, num_ids))
    F_slicing_y_times = \
            np.empty((ndims, num_ids))
    F_slicing_M_times = \
            np.empty((ndims, num_ids))

    F_indexing_times = \
            np.empty((ndims, num_ids))
    F_indexing_y_times = \
            np.empty((ndims, num_ids))
    F_indexing_M_times = \
            np.empty((ndims, num_ids))

    scipy_F_indexing_M_times = \
            np.empty((ndims, num_ids))
    scipy_F_indexing_y_times = \
            np.empty((ndims, num_ids))
    scipy_F_slicing_M_times = \
            np.empty((ndims, num_ids))
    scipy_F_slicing_y_times = \
            np.empty((ndims, num_ids))
    scipy_F_indexing_times = \
            np.empty((ndims, num_ids))
    scipy_F_slicing_times = \
            np.empty((ndims, num_ids))



    F1f_slicing_times = \
            np.empty((ndims, num_ids))
    F1f_indexing_times = \
            np.empty((ndims, num_ids))
    F1f_slicing_y_times = \
            np.empty((ndims, num_ids))
    F1f_indexing_y_times = \
            np.empty((ndims, num_ids))
    F1f_slicing_M_times = \
            np.empty((ndims, num_ids))
    F1f_indexing_M_times = \
            np.empty((ndims, num_ids))

    scipy_mat_slicing_times = \
            np.empty((ndims, num_ids))
    scipy_mat_indexing_times = \
            np.empty((ndims, num_ids))
    scipy_mat_slicing_y_times = \
            np.empty((ndims, num_ids))
    scipy_mat_indexing_y_times = \
            np.empty((ndims, num_ids))
    scipy_mat_slicing_M_times = \
            np.empty((ndims, num_ids))
    scipy_mat_indexing_M_times = \
            np.empty((ndims, num_ids))

    nruns = opts.nruns
    for i, d in enumerate(range(opts.min_dim, opts.max_dim+1)):
        F = wht(2**d)
        x = np.random.rand(F.shape[1])
        M = np.asfortranarray(np.random.rand(*(F.T.shape)))
        scipy_mat = F.factors(F.numfactors()-1)
        scipy_F = [F.factors(i) for i in range(F.numfactors())]
        F1f = Faust(scipy_mat) # single factor Faust
        for j, id_len in enumerate(id_lens):
            print("dim=", d, "slice/indexing length:", str(id_len)+'%')
            id_len = int(2**d*id_len/100)
            y = x[:id_len]
            J = [randint(0, F.shape[1]-1) for _ in range(id_len)]
            M_J = np.asfortranarray(M[J])
            x_J = np.asfortranarray(x[J])
            x_slice = np.asfortranarray(x[:id_len])
            M_slice = np.asfortranarray(M[:id_len])
            # measure time of slicing F[:, 0:id_len]
            F_slicing_times[i,j] = measure_func_time(lambda:
                                                     F[:, :id_len], nruns)
            # measure time of slicing + vec mul F[:, :id_len]@x_slice
            F_slicing_y_times[i,j] = measure_func_time(lambda:
                                                       F[:, :id_len]@x_slice,
                                                       nruns)
            # measure time of slicing + mat mul F[:, :id_len]@M_slice
            F_slicing_M_times[i,j] = measure_func_time(lambda:
                                                       F[:, :id_len]@M_slice,
                                                       nruns)
            # measure time of indexing F[:, J]
            F_indexing_times[i,j] = measure_func_time(lambda:
                                                     F[:, J], nruns)
            # measure time of indexing + vec mul F[:, J]@x_J
            F_indexing_y_times[i,j] = measure_func_time(lambda:
                                                       F[:, J]@x_J,
                                                       nruns)
            # measure time of indexing + mat mul F[:, J]@M_J
            F_indexing_M_times[i,j] = measure_func_time(lambda:
                                                       F[:, J]@M_J,
                                                       nruns)

            # measure time of scipy slicing and indexing
            scipy_F_slicing_times[i,j] = measure_func_time(lambda:
                                                     scipy_F[-1][:, :id_len], nruns)
            scipy_F_indexing_times[i,j] = measure_func_time(lambda:
                                                     scipy_F[-1][:, J], nruns)

            # measure time of slicing + vec mul F[:, :id_len]@x_slice
            scipy_F_slicing_y_times[i,j] = measure_func_time(lambda:
                                                        scipy_faust_mul_slice(scipy_F,
                                                                              x_slice,
                                                                              id_len),
                                                             nruns)
            # measure time of slicing + mat mul F[:, :id_len]@M_slice
            scipy_F_slicing_M_times[i,j] = measure_func_time(lambda:
                                                             scipy_faust_mul_slice(scipy_F,
                                                                                   M_slice,
                                                                                   id_len),
                                                             nruns)

            # measure time of indexing + vec mul scipy_F[:, J]@x_J
            scipy_F_indexing_y_times[i,j] = measure_func_time(lambda:
                                                              scipy_faust_mul_index(scipy_F,
                                                                                    x_J,
                                                                                    J),
                                                              nruns)

            # measure time of indexing + mat mul scipy_F[:, J]@M_J
            scipy_F_indexing_M_times[i,j] = measure_func_time(lambda:
                                                              scipy_faust_mul_index(scipy_F,
                                                                                    M_J,
                                                                                    J),
                                                              nruns)

            # measure time of indexing F1f[:, J]
            F1f_indexing_times[i,j] = measure_func_time(lambda:
                                                     F1f[:, J], nruns)
            # measure time of slicing F1f[:, J]
            F1f_slicing_times[i,j] = measure_func_time(lambda:
                                                       F1f[:, :id_len], nruns)

            # measure time of slicing + vec mul F1f[:, :id_len]@x_slice
            F1f_slicing_y_times[i,j] = measure_func_time(lambda:
                                                       F1f[:, :id_len]@x_slice,
                                                       nruns)
            # measure time of slicing + mat mul F1f[:, :id_len]@M_slice
            F1f_slicing_M_times[i,j] = measure_func_time(lambda:
                                                       F1f[:, :id_len]@M_slice,
                                                       nruns)
            # measure time of indexing + vec mul F1f[:, J]@x_J
            F1f_indexing_y_times[i,j] = measure_func_time(lambda:
                                                       F1f[:, J]@x_J,
                                                       nruns)
            # measure time of indexing + mat mul F1f[:, J]@M_J
            F1f_indexing_M_times[i,j] = measure_func_time(lambda:
                                                          F1f[:, J]@M_J,
                                                          nruns)
            # measure time of slicing + mat mul scipy_mat[:, :id_len]@M_slice
            scipy_mat_slicing_M_times[i,j] = measure_func_time(lambda:
                                                       scipy_mat[:, :id_len]@M_slice,
                                                       nruns)
            # measure time of slicing + mat mul scipy_mat[:, :id_len]@M_slice
            scipy_mat_slicing_y_times[i,j] = measure_func_time(lambda:
                                                       scipy_mat[:, :id_len]@x_slice,
                                                       nruns)
            # measure time of indexing + vec mul scipy_mat[:, J]@x_J
            scipy_mat_indexing_y_times[i,j] = measure_func_time(lambda:
                                                       scipy_mat[:, J]@x_J,
                                                       nruns)
            # measure time of indexing + mat mul scipy_mat[:, J]@M_J
            scipy_mat_indexing_M_times[i,j] = measure_func_time(lambda:
                                                       scipy_mat[:, J]@M_J,
                                                       nruns)
            # measure time of indexing scipy_mat[:, J]
            scipy_mat_indexing_times[i,j] = measure_func_time(lambda:
                                                     scipy_mat[:, J], nruns)
            # measure time of slicing scipy_mat[:, J]
            scipy_mat_slicing_times[i,j] = measure_func_time(lambda:
                                                       scipy_mat[:, :id_len], nruns)


    filepath = basename(argv[0]).replace('.py','')+"-d_"+str(opts.min_dim)+"_"+str(opts.max_dim)+"-"+__version__
    np.savez(filepath,
             F_slicing_times=F_slicing_times,
             F_slicing_y_times=F_slicing_y_times,
             F_slicing_M_times=F_slicing_M_times,
             F_indexing_times=F_indexing_times,
             F_indexing_y_times=F_indexing_y_times,
             F_indexing_M_times=F_indexing_M_times,
             scipy_F_indexing_times=scipy_F_indexing_times,
             scipy_F_slicing_times=scipy_F_slicing_times,
             scipy_F_slicing_y_times=scipy_F_slicing_y_times,
             scipy_F_slicing_M_times=scipy_F_slicing_M_times,
             scipy_F_indexing_M_times=scipy_F_indexing_M_times,
             scipy_F_indexing_y_times=scipy_F_indexing_y_times,
             F1f_slicing_y_times=F1f_slicing_y_times,
             F1f_slicing_M_times=F1f_slicing_M_times,
             F1f_indexing_y_times=F1f_indexing_y_times,
             F1f_indexing_M_times=F1f_indexing_M_times,
             scipy_mat_slicing_y_times=scipy_mat_slicing_y_times,
             scipy_mat_slicing_M_times=scipy_mat_slicing_M_times,
             scipy_mat_indexing_y_times=scipy_mat_indexing_y_times,
             scipy_mat_indexing_M_times=scipy_mat_indexing_M_times,
             F1f_slicing_times=F1f_slicing_times,
             F1f_indexing_times=F1f_indexing_times,
             scipy_mat_slicing_times=scipy_mat_slicing_times,
             scipy_mat_indexing_times=scipy_mat_indexing_times,
             id_lens=id_lens,
             log2_dims=list(range(opts.min_dim, opts.max_dim+1)), nruns=[nruns])
    print("output file:", filepath)
