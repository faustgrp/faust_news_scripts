from os.path import basename
from pylab import * # bad but quick
from sys import argv


if __name__ == '__main__':
    if len(argv) < 3:
        print("USAGE:", argv[0], "<npz_path1> <npz_path2>")
    npz_paths = argv[1:3]
    v = [npz_path.split('-')[-1].replace('.npz', '') for npz_path in npz_paths]
    d = [load(npz_path) for npz_path in npz_paths]
    ndims = min(len(d[0]['log2_dims']), len(d[1]['log2_dims']))
    log2_dims = d[0]['log2_dims'][:ndims]
    min_dim = log2_dims[0]
    max_dim = log2_dims[-1]

    F_slicing_times = [d_['F_slicing_times'] for d_ in d]
    F_slicing_y_times = [d_['F_slicing_y_times'] for d_ in d]
    F_slicing_M_times = [d_['F_slicing_M_times'] for d_ in d]
    F_indexing_times = [d_['F_indexing_times'] for d_ in d]
    F_indexing_y_times = [d_['F_indexing_y_times'] for d_ in d]
    F_indexing_M_times = [d_['F_indexing_M_times'] for d_ in d]

    scipy_F_indexing_times = [d_['scipy_F_indexing_times'] for d_ in d]
    scipy_F_slicing_times = [d_['scipy_F_slicing_times'] for d_ in d]
    scipy_F_slicing_y_times = [d_['scipy_F_slicing_y_times'] for d_ in d]
    scipy_F_slicing_M_times = [d_['scipy_F_slicing_M_times'] for d_ in d]
    scipy_F_indexing_y_times = [d_['scipy_F_indexing_y_times'] for d_ in d]
    scipy_F_indexing_M_times = [d_['scipy_F_indexing_M_times'] for d_ in d]

    F1f_slicing_times = [d_['F1f_slicing_times'] for d_ in d]
    F1f_indexing_times = [d_['F1f_indexing_times'] for d_ in d]
    F1f_slicing_y_times = [d_['F1f_slicing_y_times'] for d_ in d]
    F1f_slicing_M_times = [d_['F1f_slicing_M_times'] for d_ in d]
    F1f_indexing_y_times = [d_['F1f_indexing_y_times'] for d_ in d]
    F1f_indexing_M_times = [d_['F1f_indexing_M_times'] for d_ in d]

    scipy_mat_slicing_times = [d_['scipy_mat_slicing_times'] for d_ in d]
    scipy_mat_indexing_times = [d_['scipy_mat_indexing_times'] for d_ in d]
    scipy_mat_slicing_y_times = [d_['scipy_mat_slicing_y_times'] for d_ in d]
    scipy_mat_slicing_M_times = [d_['scipy_mat_slicing_M_times'] for d_ in d]
    scipy_mat_indexing_y_times = [d_['scipy_mat_indexing_y_times'] for d_ in d]
    scipy_mat_indexing_M_times = [d_['scipy_mat_indexing_M_times'] for d_ in d]

    plt.rcParams['figure.figsize'] = [15.24, 11.88]
    id_lens = d[0]['id_lens']
    assert(np.allclose(d[0]['id_lens'], d[1]['id_lens']))
    nruns = d[0]['nruns'][0]
    assert(nruns == d[1]['nruns'][0])
    w = str(len(id_lens)//2)
    print("w=", w)
    h = 2 if int(w)*2 == len(id_lens) else 3
    print("h=", h)

    suptitle("WHT Sparse Faust slicing benchmark", fontsize=20, fontweight='bold')
    fig_filepath = argv[1][:argv[1].index('-')]+"-d_"+str(min_dim)+"_"+str(max_dim)+'-1.png'

    for j, id_len in enumerate(id_lens):
        three_el_pos = int(str(h)+str(w)+str(j+1))
        subplot(three_el_pos)
        title('N = '+str(id_len)+'% of 2**log2dim')
        if j >= len(id_lens) - int(w):
            xlabel('log2dim(F)')
        if j % int(w) == 0:
            ylabel('mean time ('+str(nruns)+' runs)')

        for i, col in enumerate(['g', 'b']):
            semilogy(log2_dims, F_slicing_times[i][:,j][:ndims], col+'-o', label='F[:, :N] pyfaust'+v[i])
            semilogy(log2_dims, F_slicing_y_times[i][:,j][:ndims], col+'-^', label='F[:, :N]@x[:N] pyfaust'+v[i])
            semilogy(log2_dims, F_slicing_M_times[i][:,j][:ndims], col+'-+', label='F[:, :N]@M[:N] pyfaust'+v[i])

        semilogy(log2_dims, scipy_F_slicing_times[i][:,j][:ndims], 'y'+'-o', label='scipy F[:, :N]')
        semilogy(log2_dims, scipy_F_slicing_y_times[i][:,j][:ndims], 'y'+'-^', label='scipy F[:, :N]@x[:N]')
        semilogy(log2_dims, scipy_F_slicing_M_times[i][:,j][:ndims], 'y'+'-+', label='scipy F[:, :N]@M[:N]')

        grid(True)
        if j == 0:
            legend()
    print("figure:", fig_filepath)
    savefig(fig_filepath)

    figure()
    suptitle("WHT Sparse Faust indexing benchmark", fontsize=20, fontweight='bold')
    fig_filepath = argv[1][:argv[1].index('-')]+"-d_"+str(min_dim)+"_"+str(max_dim)+'-2.png'

    for j, id_len in enumerate(id_lens):
        three_el_pos = int(str(h)+str(w)+str(j+1))
        subplot(three_el_pos)
        title('#J = '+str(id_len)+'% of 2**log2dim')
        if j >= len(id_lens) - int(w):
            xlabel('log2dim(F)')
        if j % int(w) == 0:
            ylabel('mean time ('+str(nruns)+' runs)')

        for i, col in enumerate(['g', 'b']):
            semilogy(log2_dims, F_indexing_times[i][:,j][:ndims], col+'-o', label='F[:, J] pyfaust'+v[i])
            semilogy(log2_dims, F_indexing_y_times[i][:,j][:ndims], col+'-^', label='F[:, J]@x[J] pyfaust'+v[i])
            semilogy(log2_dims, F_indexing_M_times[i][:,j][:ndims], col+'-+', label='F[:, J]@M[J] pyfaust'+v[i])

        semilogy(log2_dims, scipy_F_indexing_times[i][:,j][:ndims], 'y'+'-o', label='scipy F[:, J]')
        semilogy(log2_dims, scipy_F_indexing_y_times[i][:,j][:ndims], 'y'+'-^', label='scipy F[:, :N]@x[:N]')
        semilogy(log2_dims, scipy_F_indexing_M_times[i][:,j][:ndims], 'y'+'-+', label='scipy F[:, J]@M[J]')

        grid(True)
        if j == 0:
            legend()
    print("figure:", fig_filepath)
    savefig(fig_filepath)

    figure()
    suptitle("scipy CSR matrix S versus F = Faust(S) indexing benchmark", fontsize=20, fontweight='bold')
    fig_filepath = argv[1][:argv[1].index('-')]+"-d_"+str(min_dim)+"_"+str(max_dim)+'-3.png'

    for j, id_len in enumerate(id_lens):
        three_el_pos = int(str(h)+str(w)+str(j+1))
        subplot(three_el_pos)
        title('#J = '+str(id_len)+'% of 2**log2dim')
        if j >= len(id_lens) - int(w):
            xlabel('log2dim(F)')
        if j % int(w) == 0:
            ylabel('mean time ('+str(nruns)+' runs)')

        for i, col in enumerate(['g', 'b']):
            semilogy(log2_dims, F1f_indexing_times[i][:,j][:ndims], col+'-o', label='F[:, J] pyfaust'+v[i])
            semilogy(log2_dims, F1f_indexing_y_times[i][:,j][:ndims], col+'-^', label='F[:, J]@x[J] pyfaust'+v[i])
            semilogy(log2_dims, F1f_indexing_M_times[i][:,j][:ndims], col+'-+', label='F[:, J]@M[J] pyfaust'+v[i])
        semilogy(log2_dims, scipy_mat_indexing_times[i][:,j][:ndims], 'y'+'-o', label='S[:, J]')
        semilogy(log2_dims, scipy_mat_indexing_y_times[0][:,j][:ndims], 'y'+'-^', label='S[:, J]@x[J]')
        semilogy(log2_dims, scipy_mat_indexing_M_times[0][:,j][:ndims], 'y'+'-+', label='S[:, J]@M[J]')
        grid(True)
        if j == 0:
            legend()
    print("figure:", fig_filepath)
    savefig(fig_filepath)

    figure()
    suptitle("scipy CSR matrix S versus F = Faust(S) slicing benchmark", fontsize=20, fontweight='bold')
    fig_filepath = argv[1][:argv[1].index('-')]+"-d_"+str(min_dim)+"_"+str(max_dim)+'-4.png'

    for j, id_len in enumerate(id_lens):
        three_el_pos = int(str(h)+str(w)+str(j+1))
        subplot(three_el_pos)
        title('N = '+str(id_len)+'% of 2**log2dim')
        if j >= len(id_lens) - int(w):
            xlabel('log2dim(F)')
        if j % int(w) == 0:
            ylabel('mean time ('+str(nruns)+' runs)')

        for i, col in enumerate(['g', 'b']):
            semilogy(log2_dims, F1f_slicing_times[i][:,j][:ndims], col+'-o', label='F[:, J] pyfaust'+v[i])
            semilogy(log2_dims, F1f_slicing_y_times[i][:,j][:ndims], col+'-^', label='F[:, :N]@x[:N] pyfaust'+v[i])
            semilogy(log2_dims, F1f_slicing_M_times[i][:,j][:ndims], col+'-+', label='F[:, :N]@M[:N] pyfaust'+v[i])
        semilogy(log2_dims, scipy_mat_slicing_times[i][:,j][:ndims], 'y'+'-o', label='S[:, J]')
        semilogy(log2_dims, scipy_mat_slicing_y_times[0][:,j][:ndims], 'y'+'-^', label='S[:, :N]@x[:N]')
        semilogy(log2_dims, scipy_mat_slicing_M_times[0][:,j][:ndims], 'y'+'-+', label='S[:, :N]@M[:N]')
        grid(True)
        if j == 0:
            legend()
    print("figure:", fig_filepath)
    savefig(fig_filepath)
